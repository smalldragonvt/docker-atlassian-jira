ARG BASE_IMAGE=library/openjdk:8-jdk-alpine
FROM $BASE_IMAGE

ARG RUN_USER                                        
ARG RUN_GROUP                                       
ARG RUN_UID                                         
ARG RUN_GID
ARG ATLASSIAN_AGENT_VERSION
ARG MYSQL_CONNECTOR_JAVA_VERSION         
ARG POSTGRES_CONNECTOR_JAVA_VERSION    
ARG JIRA_VERSION
ARG ARTEFACT_NAME=atlassian-jira-software                            

ENV RUN_USER=${RUN_USER:-jira}
ENV RUN_GROUP=${RUN_GROUP:-jira}                                    
ENV RUN_UID=${RUN_UID:-2001}
ENV RUN_GID=${RUN_GID:-2001}                                      
ENV ATLASSIAN_AGENT_VERSION={$ATLASSIAN_AGENT_VERSION:-atlassian-agent-v1.2.3}
ENV MYSQL_CONNECTOR_JAVA_VERSION=${MYSQL_CONNECTOR_JAVA_VERSION:-5.1.48}
ENV POSTGRES_CONNECTOR_JAVA_VERSION=${POSTGRES_CONNECTOR_JAVA_VERSION:-42.2.1}
ENV JIRA_VERSION=${JIRA_VERSION:-8.8.1}
ENV ARTEFACT_NAME=${ARTEFACT_NAME:-atlassian-jira-software}

# https://confluence.atlassian.com/display/JSERVERM/Important+directories+and+files
ENV JIRA_HOME                                       /var/atlassian/application-data/jira
ENV JIRA_INSTALL_DIR                                /opt/atlassian/jira

WORKDIR $JIRA_HOME

# Expose HTTP port
EXPOSE 8080

CMD ["/entrypoint.py"]
ENTRYPOINT ["/tini", "--"]

RUN apk add fontconfig python3 py3-jinja2 bash curl xz tar
RUN apk add --no-cache bash

# RUN apt-get update && apt-get upgrade -y \
#     && apt-get install -y --no-install-recommends fontconfig python3 python3-jinja2 \
#     && apt-get clean autoclean && apt-get autoremove -y && rm -rf /var/lib/apt/lists/*

ARG TINI_VERSION=v0.18.0
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /tini
RUN chmod +x /tini

ARG DOWNLOAD_URL=https://product-downloads.atlassian.com/software/jira/downloads/${ARTEFACT_NAME}-${JIRA_VERSION}.tar.gz

RUN addgroup -g ${RUN_GID} ${RUN_GROUP} \
    && adduser -u ${RUN_UID} -G ${RUN_GROUP} -h ${JIRA_HOME} -s /bin/bash -D ${RUN_USER} \
    && echo PATH=$PATH > /etc/environment \
    && mkdir -p                                        ${JIRA_INSTALL_DIR} \
    && curl -Ls                                     ${DOWNLOAD_URL} | tar -xz --directory "${JIRA_INSTALL_DIR}" --strip-components=1 --no-same-owner \
    && curl -Ls                "https://dev.mysql.com/get/Downloads/Connector-J/mysql-connector-java-${MYSQL_CONNECTOR_JAVA_VERSION}.tar.gz" | tar -xz --directory "${JIRA_INSTALL_DIR}/lib" --strip-components=1 --no-same-owner "mysql-connector-java-${MYSQL_CONNECTOR_JAVA_VERSION}/mysql-connector-java-${MYSQL_CONNECTOR_JAVA_VERSION}-bin.jar" \
    && rm -f                   "${JIRA_INSTALL_DIR}/lib/postgresql-9.1-903.jdbc4-atlassian-hosted.jar" \
    && curl -Ls                "https://jdbc.postgresql.org/download/postgresql-${POSTGRES_CONNECTOR_JAVA_VERSION}.jar" -o "${JIRA_INSTALL_DIR}/lib/postgresql-${POSTGRES_CONNECTOR_JAVA_VERSION}.jar" \
    && curl -Ls                "https://onedrive.live.com/download?cid=4752CC08BF9C87EB&resid=4752CC08BF9C87EB%2111640&authkey=AFiEPulz5Ui8BCY" | tar -xz --directory "/" --strip-components=1 --no-same-owner "atlassian-agent-v1.2.3/atlassian-agent.jar" \
    && chmod -R "u=rwX,g=rX,o=rX"                      ${JIRA_INSTALL_DIR}/ \
    && chown -R root.                               ${JIRA_INSTALL_DIR}/ \
    && chown -R ${RUN_USER}:${RUN_GROUP}            ${JIRA_INSTALL_DIR}/logs \
    && chown -R ${RUN_USER}:${RUN_GROUP}            ${JIRA_INSTALL_DIR}/temp \
    && chown -R ${RUN_USER}:${RUN_GROUP}            ${JIRA_INSTALL_DIR}/work \
    \
    && sed -i -e 's/^JVM_SUPPORT_RECOMMENDED_ARGS=""$/: \${JVM_SUPPORT_RECOMMENDED_ARGS:=""}/g' ${JIRA_INSTALL_DIR}/bin/setenv.sh \
    && sed -i -e 's/^JVM_\(.*\)_MEMORY="\(.*\)"$/: \${JVM_\1_MEMORY:=\2}/g' ${JIRA_INSTALL_DIR}/bin/setenv.sh \
    && sed -i -e 's/-XX:ReservedCodeCacheSize=\([0-9]\+[kmg]\)/-XX:ReservedCodeCacheSize=${JVM_RESERVED_CODE_CACHE_SIZE:=\1}/g' ${JIRA_INSTALL_DIR}/bin/setenv.sh \
    \
    && touch /etc/container_id \
    && chown ${RUN_USER}:${RUN_GROUP}               /etc/container_id \
    && chown -R ${RUN_USER}:${RUN_GROUP}            ${JIRA_HOME}

VOLUME ["${JIRA_HOME}"] # Must be declared after setting perms

COPY entrypoint.py \
     shared-components/image/entrypoint_helpers.py  /
COPY shared-components/support                      /opt/atlassian/support
COPY config/*                                       /opt/atlassian/etc/
